'use strict';

import {Intro} from '../intro';
import {Form} from '../form';
import {CTA} from '../cta';

import config from '../../../config.json';

import './styles.scss';

export function Widget(operation) {
  const widgetElement = document.createElement('div');
  widgetElement.classList.add('widget');

  function applyTheme(theme) {
    if ((theme.intro || {}).background)
      widgetElement.style.setProperty('--intro-background', theme.intro.background);
    if ((theme.intro || {}).text)
      widgetElement.style.setProperty('--intro-color', theme.intro.text);
    if ((theme.form || {}).background)
      widgetElement.style.setProperty('--form-background', theme.form.background);
    if (theme.input)
      widgetElement.style.setProperty('--input-color', theme.input);
  }

  function displayIll(illObj) {
    const imgElement = document.createElement('img');
    imgElement.classList.add('widget__ill');
    imgElement.setAttribute('src', illObj.src);
    if (illObj.alt) imgElement.setAttribute('alt', illObj.alt);
    if (illObj.sizes) imgElement.setAttribute('sizes', illObj.sizes);
    if (illObj.srcset) imgElement.setAttribute('srcset', illObj.srcset);
    widgetElement.appendChild(imgElement);
  }

  const intro = Intro();

  const introElement = intro.element;
  introElement.classList.add('widget__intro');
  widgetElement.appendChild(introElement);

  if (operation.partners) {
    intro.displayPartners(operation.partners);
  }

  if (operation.title) {
    intro.displayTitle(operation.title);
  }

  if (operation.subtitle) {
    intro.displaySubtitle(operation.subtitle);
  }

  if (operation.quote) {
    intro.displayQuote(operation.quote);
  }

  if ((operation.ill || {}).src) {
    displayIll(operation.ill);
  }

  if ((operation.inputs || {}).length) {
    const formElement = Form(operation.id, operation.inputs, operation.confirmationMessage || config.api.confirmationMessage);
    formElement.classList.add('widget__form');
    widgetElement.appendChild(formElement);
  }

  if(operation.cta) {
    const ctaElement = CTA(operation.cta.label, operation.cta.href, operation.cta.target);
    ctaElement.classList.add('widget__cta');
    widgetElement.appendChild(ctaElement);
  }

  if (operation.theme) {
    applyTheme(operation.theme);
  }

  return widgetElement;
}
