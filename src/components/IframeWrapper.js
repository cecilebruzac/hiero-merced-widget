'use strict';

import {ReCAPTCHA} from '../utils/ReCAPTCHA';

export function IframeWrapper(contentElement, stylesheetPath, edgingColor) {
  const iframeElement = document.createElement('iframe');
  iframeElement.src = 'about:blank';
  iframeElement.setAttribute('height', '0px');
  iframeElement.setAttribute('width', '500px');
  iframeElement.setAttribute('frameborder', '0');
  iframeElement.setAttribute('scrolling', 'no');
  iframeElement.setAttribute('allowfullscreen', 'true');
  iframeElement.setAttribute('allowtransparency', 'true');
  iframeElement.setAttribute('border', '0');
  iframeElement.setAttribute('cellspacing', '0');
  iframeElement.style.display = 'block';
  iframeElement.style.maxWidth = '100%';
  iframeElement.style.minWidth = '220px';

  if(edgingColor) {
    iframeElement.style.backgroundColor = edgingColor;
  }

  iframeElement.addEventListener('load', function () {

    const reCAPTCHA = ReCAPTCHA();
    const recaptchaApiScriptElement = reCAPTCHA.scriptElement();

    window.onReCAPTCHALoadCallback = function () {
      reCAPTCHA.onLoadCallback(function (reCAPTCHAToken) {
        document.dispatchEvent(new CustomEvent('getReCAPTCHAToken', {detail: {reCAPTCHAToken: reCAPTCHAToken}}));
      });
    };

    document.head.appendChild(recaptchaApiScriptElement);

    const resetCSSLinkElement = document.createElement('link');
    resetCSSLinkElement.setAttribute('rel', 'stylesheet');
    resetCSSLinkElement.setAttribute('href', 'https://unpkg.com/reset-css@4.0.1/reset.css');
    iframeElement.contentDocument.head.appendChild(resetCSSLinkElement);

    if (stylesheetPath) {
      const linkElement = document.createElement('link');
      linkElement.setAttribute('rel', 'stylesheet');
      linkElement.setAttribute('href', stylesheetPath);
      iframeElement.contentDocument.head.appendChild(linkElement);
    }

    const marginValue = 5;
    contentElement.style.margin = marginValue + 'px';
    iframeElement.contentDocument.body.appendChild(contentElement);

    function adaptIframeHeight() {
      iframeElement.setAttribute('height', contentElement.offsetHeight + (2 * marginValue) + 'px');
    }

    setTimeout(function () {
      adaptIframeHeight();
      window.addEventListener('resize', adaptIframeHeight, false);
      document.addEventListener('updateContent', adaptIframeHeight);
    }, 500);

  }, false);

  return iframeElement;
}