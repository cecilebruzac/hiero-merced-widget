'use strict';

import './styles.scss';

export function CTA(label, href, target) {
  const ctaElement = document.createElement('p');
  const ctaLinkElement = document.createElement('a');

  ctaLinkElement.classList.add('cta');
  ctaLinkElement.setAttribute('href', href);
  ctaLinkElement.setAttribute('target', target);
  ctaLinkElement.innerHTML = label;
  ctaElement.append(ctaLinkElement);

  return ctaElement;
}