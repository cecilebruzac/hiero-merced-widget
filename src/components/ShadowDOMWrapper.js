'use strict';

import {ReCAPTCHA} from '../utils/ReCAPTCHA';

export function ShadowDOMWrapper(contentElement, stylesheetPath, edgingColor) {
  const wrapperElement = document.createElement('hiero-merced-widget');
  wrapperElement.setAttribute('style', 'display:block;box-sizing:border-box;max-width:100%;width:500px;min-width:220px;margin:10px 0;' + (edgingColor ? ('padding:5px;background-color:' + edgingColor) : ''));

  const shadowRoot = wrapperElement.attachShadow({mode: 'open'});

  contentElement.style.display = 'none';

  if (stylesheetPath) {
    const linkElement = document.createElement('link');
    linkElement.setAttribute('rel', 'stylesheet');
    linkElement.setAttribute('href', stylesheetPath);
    shadowRoot.appendChild(linkElement);
  }

  shadowRoot.appendChild(contentElement);

  const reCAPTCHA = ReCAPTCHA();
  const recaptchaApiScriptElement = reCAPTCHA.scriptElement();

  window.onReCAPTCHALoadCallback = function () {
    reCAPTCHA.onLoadCallback(function (reCAPTCHAToken) {
      document.dispatchEvent(new CustomEvent('getReCAPTCHAToken', {detail: {reCAPTCHAToken: reCAPTCHAToken}}));
    });
  };

  shadowRoot.appendChild(recaptchaApiScriptElement);

  return wrapperElement;
}