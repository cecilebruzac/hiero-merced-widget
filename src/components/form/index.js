'use strict';

import config from '../../../config';

import {convertedFormValuesToJSON} from '../../utils/convertedFormValuesToJSON';

import {postResponse} from '../../api/postResponse';
import {getResponses} from '../../api/getResponses';
import {postDataForAudienceAnalysis} from '../../api/postDataForAudienceAnalysis';

import {FormInputFragment} from './FormInputFragment';
import {Spinner} from '../spinner';

import {Session} from './../../session';

import './styles.scss';

export function Form(operationId, inputs, confirmationMessage) {
  const hostname = window.location.hostname;

  const formElement = document.createElement('form');
  formElement.classList.add('form');

  const spinnerElement = Spinner();
  spinnerElement.classList.add('form__spinner');
  let isSpinnerDisplayed = false;

  function showErrorMessage() {
    removeConfirmationMessage();
    removeErrorMessage();

    const errorMessageElement = document.createElement('p');
    errorMessageElement.classList.add('form__error');
    errorMessageElement.innerHTML = config.api.errorMessage;
    formElement.appendChild(errorMessageElement);
    document.dispatchEvent(new CustomEvent('updateContent', {}));
  }

  function removeErrorMessage() {
    const errorMessageElements = formElement.getElementsByClassName('form__error');
    while (errorMessageElements.length > 0) errorMessageElements[0].remove();
    document.dispatchEvent(new CustomEvent('updateContent', {}));
  }

  function showConfirmationMessage() {
    removeConfirmationMessage();
    removeErrorMessage();

    const confirmationMessageElement = document.createElement('p');
    confirmationMessageElement.classList.add('form__confirmation');
    confirmationMessageElement.innerHTML = confirmationMessage;
    formElement.appendChild(confirmationMessageElement);
    document.dispatchEvent(new CustomEvent('updateContent', {}));
  }

  function removeConfirmationMessage() {
    const confirmationMessageElements = formElement.getElementsByClassName('form__confirmation');
    while (confirmationMessageElements.length > 0) confirmationMessageElements[0].remove();
    document.dispatchEvent(new CustomEvent('updateContent', {}));
  }

  function submit(operationId, callback) {

    if (Session && typeof Session === 'function' && Session.hostname && Session.userPublicIp) {
      postDataForAudienceAnalysis(operationId, 'onSubmitForm', Session.hostname, Session.userPublicIp);
    }

    if (!isSpinnerDisplayed) {
      formElement.appendChild(spinnerElement);
      isSpinnerDisplayed = true;
      document.dispatchEvent(new CustomEvent('updateContent', {}));
    }

    postResponse(operationId, hostname, convertedFormValuesToJSON(formElement), function (response) {

      if (isSpinnerDisplayed) {
        spinnerElement.parentNode.removeChild(spinnerElement);
        isSpinnerDisplayed = false;
        document.dispatchEvent(new CustomEvent('updateContent', {}));
      }

      if (response === 'error') {
        showErrorMessage();
      } else {
        showConfirmationMessage();

        if (Session && typeof Session === 'function' && Session.hostname && Session.userPublicIp) {
          postDataForAudienceAnalysis(operationId, 'formSubmitted', Session.hostname, Session.userPublicIp);
        }

        getResponses(operationId, function (response) {
          //console.log(response);
          callback();
        });
      }
    });
  }

  inputs.forEach(function (input) {
    formElement.appendChild(FormInputFragment(input));
  });

  const reCAPTCHABadgeElement = document.createElement('p');
  reCAPTCHABadgeElement.classList.add('form__recaptcha-badge');
  reCAPTCHABadgeElement.innerHTML = config.reCAPTCHABadgeText;
  formElement.append(reCAPTCHABadgeElement);

  const reCAPTCHAInputElement = document.createElement('input');
  reCAPTCHAInputElement.setAttribute('type', 'hidden');
  reCAPTCHAInputElement.setAttribute('name', 'reCAPTCHAToken');
  reCAPTCHAInputElement.setAttribute('value', 'validated');
  formElement.prepend(reCAPTCHAInputElement);

  document.addEventListener('getReCAPTCHAToken', function (event) {
    reCAPTCHAInputElement.setAttribute('value', event.detail.reCAPTCHAToken);
  });

  formElement.addEventListener('submit', function (event) {
    event.preventDefault();

    submit(operationId, function () {
      reCAPTCHAInputElement.setAttribute('value', 'validated');
    });
  });

  return formElement;
}
