'use strict';

export const isInViewport = function (element) {
  const rect = element.getBoundingClientRect();
  const windowHeight = (window.innerHeight || document.documentElement.clientHeight);
  const windowWidth = (window.innerWidth || document.documentElement.clientWidth);

  const isRectVisibleInTheVertical = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
  const isRectVisibleInTheHorizontal = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

  return (isRectVisibleInTheVertical && isRectVisibleInTheHorizontal);
};
