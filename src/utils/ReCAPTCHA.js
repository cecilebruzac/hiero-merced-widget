'use strict';

import config from '../../config';
import {id} from '../utils/id';

export function ReCAPTCHA() {

  function scriptElement() {
    const script = document.createElement('script');
    script.src = 'https://www.google.com/recaptcha/api.js?render=explicit&onload=onReCAPTCHALoadCallback';
    script.setAttribute('async', '');
    script.setAttribute('defer', '');
    return script;
  }

  function onLoadCallback(callback) {
    if (typeof grecaptcha === 'object') {
      const badge = document.createElement('div');
      const badgeid = id();
      badge.setAttribute('id', badgeid);
      badge.style.display = 'none';
      document.body.append(badge);

      const clientId = grecaptcha.render(badgeid, {
        'sitekey': config.reCAPTCHASiteKey,
        'badge': 'inline',
        'size': 'invisible'
      });

      grecaptcha.ready(function () {
        grecaptcha.execute(clientId, {action: 'widget'}).then(function (reCAPTCHAToken) {
          callback(String(reCAPTCHAToken));
        });
      });
    }
  }

  return {
    scriptElement: scriptElement,
    onLoadCallback: onLoadCallback
  }
}
