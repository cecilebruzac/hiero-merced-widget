'use strict';

import config from '../../config.json';

export function getResponses(operationId, callback) {
  const route = config.api.url + '/operation/' + operationId + '/responses';
  const xhr = new XMLHttpRequest();
  xhr.open('GET', route, true);

  xhr.onreadystatechange = function () {
    if (this.status === 200) {
      if (this.readyState === XMLHttpRequest.DONE) {
        const response = JSON.parse(xhr.responseText);
        if (callback && typeof callback === 'function') callback(response);
      } else {
        if (callback && typeof callback === 'function') callback('pending');
      }
    } else {
      if (callback && typeof callback === 'function') callback('error');
    }
  };

  xhr.setRequestHeader('Authorization', 'Bearer ' + config.api.token);
  xhr.send(null);
}
